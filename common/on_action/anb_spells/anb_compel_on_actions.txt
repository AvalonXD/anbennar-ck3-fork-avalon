﻿
### EVENT ON_ACTIONS FOR COMPEL ###

anb_compel_ongoing = {
	trigger = {
		exists = scope:scheme
	}

	random_events = {
		chance_to_happen = 10

		# Spell specific events #
		100 = anb_compel_ongoing.1 # Know thy Enemy
		
		# General spellcasting events #
		50 = anb_spellcasting_general_ongoing_events.1 # Passage in an ancient lorebook
		50 = anb_spellcasting_general_ongoing_events.2 # Stress of casting magic leaves a toll on your health
		#50 = anb_spellcasting_general_ongoing_events.3 # Target starts to have doubts
		#50 = anb_spellcasting_general_ongoing_events.4 # Ingest Damestear potion - need to disable/change second option for hostile spells
	}
}
